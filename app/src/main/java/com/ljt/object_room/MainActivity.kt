package com.ljt.object_room

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.ljt.object_room.databinding.ActivityMainBinding
import com.ljt.object_room.view.CustomView
import kotlinx.coroutines.*
import java.lang.Thread.sleep
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding

    private lateinit var thread: MyThread

    private var mIndex: Int = 0
    private var mReturn: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.roomTitleView.ivModuleBack.setOnClickListener(this)
        binding.tvModuleStopThread.setOnClickListener(this)

        /**
         * 设定动画
         */
        //        val objectAnimator:ValueAnimator = ValueAnimator.ofFloat(0F, 1F)
        //        objectAnimator.duration = 3000
        //        objectAnimator.repeatCount = ValueAnimator.INFINITE
        //        objectAnimator.addUpdateListener {
        //            val value = it.animatedValue as Float
        //            binding.cvModule.setCircleSecondCenter((value * 360).roundToInt())
        //        }
        //开启动画
        //objectAnimator.start()


        /**
         * 协程
         */
        //        val scope = CoroutineScope(Dispatchers.IO)
        //        scope.launch {
        //            Log.e("TAG","请开始您的表演")
        //            delay(3000)
        //            Log.e("TAG","请结束您的表演")
        //        }
        //        Log.e("TAG","线程状态${if(scope.isActive){"正在运行"}else{"已结束"}}")

        //协程取消并等待所有协程完成
        //scope.cancelAndJoin()
        //协程取消
        //scope.cancel()

        /**
         * launch会返回一个job对象，用于控制和取消协程
         * async会返回一个deferred对象，用于获取协程的结果
         */
        //        val result = async{
        //            return@async "scope is over"
        //        }
        //        Log.e("TAG","result is ${result.await()}")

        //        val thread = Thread {
        //            while (!Thread.currentThread().isInterrupted){
        //                Log.e("TAG","thread开始运行")
        //                if (Thread.interrupted()){
        //                    Log.e("TAG","thread结束运行")
        //                    return@Thread
        //                }
        //            }
        //        }

        thread = MyThread()
        thread.start()
        Log.e(
            "TAG", "thread状态${
                if (thread.isAlive) {
                    "仍存活"
                } else {
                    "已毁灭"
                }
            }"
        )
        //thread.join()
        //        thread.interrupt()
        //        Log.e(
        //            "TAG", "thread状态${
        //                if (thread.isInterrupted) {
        //                    "线程中断"
        //                } else {
        //                    "线程未中断"
        //                }
        //            }"
        //        )
    }

    class MyThread : Thread() {
        override fun run() {
            while (!currentThread().isInterrupted) {

                Log.e("TAG", "thread is running ${System.currentTimeMillis()}")

                if (interrupted()) {
                    Log.e("TAG", "thread结束运行")
                    return@run
                }
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    override fun onClick(v: View) {
        val id = v.id
        when (id) {
            R.id.ivModuleBack -> {
                finish()
                Log.e("TAG", "返回")
            }

            R.id.tvModuleStopThread -> {
                Toast.makeText(this, "线程调停点击", Toast.LENGTH_SHORT).show()
                Log.e("TAG", "线程调停点击->" + (mIndex++))
                thread.interrupt()

                Log.e(
                    "TAG", "thread状态${
                        if (thread.isInterrupted) {
                            "线程中断"
                        } else {
                            "线程未中断"
                        }
                    }"
                )
            }
        }
    }
}