package com.ljt.object_room

import android.app.Application
/**
 * @auther :lujiantao
 * @date :2024/6/6 3:20 PM
 * @desc :
 */
public class RoomApplicaton:Application() {

    companion object {
        @Volatile
        private var instance: RoomApplicaton? = null

        fun getInstance(): RoomApplicaton {
            return instance ?: synchronized(this) {
                instance ?: RoomApplicaton().also { instance = it }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}