package com.ljt.object_room.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${ljt} on 2021/12/15
 *
 * @author ljt
 * @desc 随随便便绘制一个圆
 */
public class CustomView extends View {

    Context context;
    Paint paint;

    List<PointF> circlePoints = new ArrayList<>();
    Float centerX,centerY,centerSecondX,centerSecondY,radius;
    float angle = 0;


    public CustomView(Context context) {
        super(context);
        this.context = context;
        // 在这里可以进行一些初始化操作
        init();
    }


    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        // 在这里可以进行一些初始化操作
        init();
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        // 在这里可以进行一些初始化操作
        init();
    }


    private void init() {
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(10);



    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // 设置测量的维度
        /**
         * 必须在onMeasure方法中调用SetMeasuredDimension方法
         * 否则在布局文件中设置的宽高无效，会报错：
         * java.lang.IllegalStateException:
         * View with id 2131231097:
         * com.ljt.object_room.view.CustomView#onMeasure()
         * did not set the measured dimension by calling setMeasuredDimension()
         *
         * getDefaultSize():返回实用程序返回默认大小
         * getSuggestedMinimumWidth():返回视图应使用的建议最小宽度
         * getSuggestedMinimumHeight():返回视图应用使用的建议最小高度
         */
        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec), getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.e("TAG", "onDraw:width-->" + getWidth() + "height-->" + getHeight());
        Log.e("TAG", "onDraw:radius-->" + Math.min(getWidth(), getHeight()) / 4);

        canvas.save();

        //计算第一个圆圆心
        centerX = (float) (getWidth() / 2);
        centerY = (float) (getHeight() / 2);
        radius = 100F;



        // 计算圆上的每个点
        if (circlePoints.isEmpty()) {
            centerSecondX = centerX - 200;
            centerSecondY = centerY;
            for (int i = 0; i < 360; i++) {
                double angle = i * Math.PI / 180;
                double x = centerX + radius * Math.cos(angle);
                double y = centerY + radius * Math.sin(angle);

                // 添加到列表中
                PointF point = new PointF((float) x, (float) y);
                circlePoints.add(point);
                Log.e("TAG","圆周边轨迹init:x-->"+x+"y-->"+y);
            }
        }

        // 绘制一个圆
        canvas.drawCircle(centerX, centerY, radius, paint);

        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);
        // 绘制第二个圆
        canvas.drawCircle(centerSecondX, centerSecondY, radius, paint);

        /**
         * 此调用平衡了以前对save（）的调用，
         * 用于删除自上次save调用以来对矩阵/剪辑状态的所有修改。
         * 调用restore（）的次数多于调用save（）的时间，这是一个错误。
         */
        canvas.restore();
    }

    /**
     * 动态设置外圆圆心，并重新绘制
     * @param index
     */
    public void setCircleSecondCenter(int index) {

        if (index < 0 || index >= circlePoints.size()){
            Log.e("TAG", "setCircleSecondCenter:index-->" + index);
            return;
        }

        if (!circlePoints.isEmpty()) {
            centerSecondX = circlePoints.get(index).x;
            centerSecondY = circlePoints.get(index).y;
        }else{
            centerSecondX = centerX - 200;
            centerSecondY = centerY;
        }
        invalidate();
    }
}
