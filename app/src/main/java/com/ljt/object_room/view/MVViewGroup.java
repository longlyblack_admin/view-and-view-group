package com.ljt.object_room.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class MVViewGroup extends ViewGroup {

    public int mWidth;
    public int mHeight;

    public MVViewGroup(Context context) {
        this(context, null);
    }

    public MVViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MVViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //测量
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        //获取子View的个数
        int childCount = getChildCount();
        Log.e("TAG", "子View的个数：" + childCount);

        /**
         * 思考
         * 1 自定义viewGroup应该从那些方面进行考虑？
         * 2 如果viewGroup中没有任何控件，怎么设置？
         * 3 如果viewGroup中有控件，怎么设置？
         */
        if (childCount == 0){
            //如果没有子view，那么viewGroup的宽高就是xml界面设置的宽高
            setMeasuredDimension(measureWidthOrHeight(widthMeasureSpec),measureWidthOrHeight(heightMeasureSpec));
        }else{
            //如果有子view，那么viewGroup的宽度就是所有子view的高度之和，viewGroup的宽度就是所有子view中最宽的View宽度值
            int childViewWidth = 0;
            int childViewHeight = 0;

            int childViewMarginLeft = 0;
            int childViewMarginRight = 0;
            int childViewMarginTop = 0;
            int childViewMarginBottom = 0;

            for(int i = 0;i < childCount;i++){
                View childView = getChildAt(i);
                //1 测量子view
                measureChild(childView,widthMeasureSpec,heightMeasureSpec);

                //2 获取子view的宽高
                //viewGroup的宽度就是所有子view的高度之和，viewGroup的宽度就是所有子view中最宽的View宽度值
                childViewWidth = Math.max(childViewWidth,childView.getMeasuredWidth());
                childViewHeight += childView.getMeasuredHeight();

                //3 获取子view的margin值
                MarginLayoutParams lp = (MarginLayoutParams) childView.getLayoutParams();
                childViewMarginLeft = Math.max(childViewMarginLeft,lp.leftMargin);
                childViewMarginRight = Math.max(childViewMarginRight,lp.rightMargin);
                childViewMarginTop += lp.topMargin;
                childViewMarginBottom += lp.bottomMargin;

                //4 设置子view的布局参数
                mWidth = childViewWidth + childViewMarginLeft + childViewMarginRight;
                mHeight = childViewHeight + childViewMarginTop + childViewMarginBottom;

                //5 设置子view的布局参数
                setMeasuredDimension(measureWidthOrHeight(widthMeasureSpec,mWidth),measureWidthOrHeight(heightMeasureSpec,mHeight));
            }
        }
    }

    //子view为零，那么viewGroup的宽高就是xml界面设置的宽高
    public int measureWidthOrHeight(int measureSpec){
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY){
            result = specSize;
        }
        return result;
    }

    //子view不为零
    public int measureWidthOrHeight(int measureSpec,int widthOrHeight){
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if(specMode == MeasureSpec.EXACTLY){
            result = specSize;
        }else{
            result = widthOrHeight;
        }
        return result;
    }

    //布局
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        //布局子空间
        int left,right,top,bottom;
        int childCount = getChildCount();
        int countTop = 0;
        for(int i = 0;i < childCount;i++){
            View childView = getChildAt(i);
            //获取子view的布局参数
            MarginLayoutParams lp = (MarginLayoutParams)childView.getLayoutParams();
            left = lp.leftMargin;
            right = left + childView.getMeasuredWidth();
            top = countTop + lp.topMargin;
            bottom = top + childView.getMeasuredHeight();

            childView.layout(left,top,right,bottom);
            countTop += (bottom - top) + lp.topMargin + lp.bottomMargin;
        }
    }

    /**
     * 当使用到childView.getLayoutParams()方法时，需要重写generateLayoutParams方法
     * 否则会报类型转换异常的错误
     * @param attributeSet the attributes to build the layout parameters from
     *
     * @return
     */
    @Override
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new MarginLayoutParams(getContext(),attributeSet);
    }

    //绘制,但如果是自定义ViewGroup，那么onDraw()方法是无效的，应该使用dispatchDraw()方法
    //    @Override
    //    protected void onDraw(Canvas canvas) {
    //        super.onDraw(canvas);
    //    }
}
