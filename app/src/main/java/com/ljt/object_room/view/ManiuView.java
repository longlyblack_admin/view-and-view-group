package com.ljt.object_room.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import androidx.annotation.Nullable;
import com.ljt.object_room.R;

public class ManiuView extends View {

    public Context context;
    public String text;
    public int color;

    private Paint mPaint;
    private Rect mRect;
    private int mWidth;
    private int mHeight;

    public ManiuView(Context context) {
        this(context,null);
    }

    public ManiuView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ManiuView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ManiuView);
        text = ta.getString(R.styleable.ManiuView_maniu_text);
        color = ta.getColor(R.styleable.ManiuView_maniu_text_color, 0x000000);
        ta.recycle();

        Log.e("TAG","text->"+ text);

        //初始化画笔
        mPaint = new Paint();
        mPaint.setColor(color);
        mRect = new Rect();
        mPaint.setTextSize(50);
        mPaint.getTextBounds(text, 0, text.length(), mRect);
    }

    /**
     *
     * @param widthMeasureSpec horizontal space requirements as imposed by the parent.
     *                         The requirements are encoded with
     *                         {@link android.view.View.MeasureSpec}.
     * @param heightMeasureSpec vertical space requirements as imposed by the parent.
     *                         The requirements are encoded with
     *                         {@link android.view.View.MeasureSpec}.
     *
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        /**
         * 根据xml文件中view的宽度设置情况来设置自定义view的宽度
         * exactly /ɪɡˈzæktli/ 确切的  match_parent 或者 100dpd等
         * AT_MOST /əˈtɒm/ 最多
         */
        int specModel = MeasureSpec.getMode(widthMeasureSpec);
        //specModel->1073741824
        Log.e("TAG","specModel->"+ specModel);
        int specWidth = MeasureSpec.getSize(widthMeasureSpec);
        //spectWidth->263、394、656
        Log.e("TAG","spectWidth->" + specWidth);
        if (specModel == MeasureSpec.EXACTLY) {
            mWidth = specWidth;
        }else{
            mWidth = getPaddingLeft() + mRect.width() + getPaddingRight();
        }

        //根据xml文件中view的高度设置情况来设置自定义view的高度
        int specHeight = MeasureSpec.getSize(heightMeasureSpec);

        if (specModel == MeasureSpec.EXACTLY) {
            mHeight = specHeight;
        }else{
            mHeight = getPaddingTop() + mRect.height() + getPaddingBottom();
        }

        //将动态获取的宽高设置给View
        setMeasuredDimension(mWidth, mHeight);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        //设置自定义textView独有的属性基线baseLine
        //        //X坐标
        //        int dx = getWidth() / 2 - mRect.width() / 2;
        //        //FontMetricsInt 字体度量转换成int的工具
        //        Paint.FontMetricsInt fontMetricsInt = mPaint.getFontMetricsInt();
        //        int dy = (fontMetricsInt.bottom - fontMetricsInt.top) / 2 - fontMetricsInt.bottom;
        //        int baseLine = getHeight() / 2 + dy;
        //        canvas.drawText(text, getPaddingLeft() + dx, getPaddingTop() + baseLine + mRect.height(), mPaint);
        canvas.drawText(text, getPaddingLeft(), getPaddingTop() + mRect.height(), mPaint);

        canvas.restore();
    }


    public void init() {

    }
}
