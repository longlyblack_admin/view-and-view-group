package com.ljt.object_room.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.ljt.object_room.R

/**
 * 自定义title
 * @author LiJingTao
 * @email 1007687560@qq.com
 * @date 2022-08-05 14:06
 */
public class RoomTitleView(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    lateinit var ivModuleBack:ImageView
    lateinit var tvModuleTitle:TextView
    lateinit var llModule:RelativeLayout

    var tvModuleText:String = ""
    var tvModuleColor:Int = 0
    var llModuleBackgroundColor:Int = 0

    init {
        var typedArray: TypedArray = context!!.obtainStyledAttributes(attrs, R.styleable.RoomTitleView)
        tvModuleText = typedArray.getString(R.styleable.RoomTitleView_title_text).toString()
        tvModuleColor = typedArray.getColor(R.styleable.RoomTitleView_title_text_color, Color.BLACK)
        llModuleBackgroundColor = typedArray.getColor(R.styleable.RoomTitleView_title_background,Color.WHITE)
        typedArray.recycle()
        initView()
    }

    private fun initView() {
        LayoutInflater.from(context).inflate(R.layout.view_title,this,true)
        llModule = findViewById(R.id.llModule)
        ivModuleBack = findViewById(R.id.ivModuleBack)
        tvModuleTitle = findViewById(R.id.tvModuleTitle)

        llModule.setBackgroundColor(llModuleBackgroundColor)
        tvModuleTitle.text = tvModuleText
        tvModuleTitle.setTextColor(tvModuleColor)

    }

    /**
     * 返回点击
     * @param listener View.OnClickListener
     */
    public fun setBackClickListener(listener: View.OnClickListener){
        ivModuleBack.setOnClickListener(listener)
    }


}